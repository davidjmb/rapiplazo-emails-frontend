var gulp 			= require("gulp"),
	plumber 		= require('gulp-plumber'),
	fs 				= require('fs'),
	browserSync 	= require('browser-sync').create(),
	jade 			= require("gulp-jade"),
	prettify 		= require("gulp-prettify"),
	styl 			= require("gulp-stylus"),
	rupture 		= require("rupture"),
	nib 			= require("nib"),
	sourcemaps 		= require('gulp-sourcemaps'),
	minifyCSS 		= require('gulp-minify-css'),
	imageop 		= require('gulp-image-optimization'),
	rename 			= require('gulp-rename'),
	notify 			= require('gulp-notify'),
	changed 		= require('gulp-changed'),
	deleted 		= require('gulp-deleted'),
	collate 		= require('gulp-collate'),
	concat 			= require('gulp-concat'),
	uglify 			= require('gulp-uglify'),
	order 			= require('gulp-order'),
	header	 		= require('gulp-header'),
	CacheBuster		= require('gulp-cachebust'),
	environments	= require('gulp-environments'),
	inlineCss 		= require('gulp-inline-css');



/*
	Comando para instalar dependencia:
	npm install --save-dev [dependencia]
	npm i -D [dependencia]
*/




/**
 *
 * Variables para ambiente desarrollo y producción
 *
 * Ejecutar ambiente desarrollo: gulp --env development
 * Ejecutar ambiente producción: gulp --env production
 *
 */
var desarrollo = environments.development,
	produccion = environments.production;



// Configuración de rutas de archivos
var carpeta = {

	fuente: "src",
	destino: "dist"

}

var ruta = {

	estilos: {
		src: carpeta.fuente + "/styl/global.styl",
		watch: carpeta.fuente + "/styl/**/*.styl",
		dist: carpeta.destino + "/assets/css"
	},
	js: {
		src: carpeta.fuente + "/js/**/*.js",
		dist: carpeta.destino + "/assets/js"
	},
	imagenes: {
		src: carpeta.fuente + "/images/**/*.{jpg,jpeg,png,gif,JPG,JPEG}",
		dist: carpeta.destino + "/assets/images"
	},
	tpl: {
		src: carpeta.fuente + "/tpl/*.jade"
	}

}

// Obtener el año actual
var fecha = new Date(),
	ano = fecha.getFullYear();


var pkg = require('./package.json');

// Generar un "Hash" de 10 caracteres
var cachebust = new CacheBuster({

		checksumLength: 10

});



/*	SERVIDOR
----------------------------------------------------------------------*/
gulp.task('servidor', function() {

    browserSync.init({
        server: carpeta.destino, // Carpeta del servidor
        // proxy: 'framework:8888', // Servidor Local
        open: false,
        notify: false
    });

});




/*	IMAGENES
----------------------------------------------------------------------*/
gulp.task( "imagenes",  function(){

	gulp.src( ruta.imagenes.src )
		
		// Cambiar de imagen.jpg -> imagen.[ID].jpg
		.pipe( produccion( cachebust.resources() ) )

		/**
		 *
		 * == collate(), deleted() y changed()
		 * Verificar si existe un cambio o se ha eliminado el archivo 
		 * para efectuar su cambio o eliminación de la carpeta /dist
		 *
		 */
		.pipe( collate( carpeta.fuente + "/images" ) )
		.pipe( deleted( ruta.imagenes.dist, ["**/*.{jpg,jpeg,png,gif,JPG,JPEG}"]) )
		.pipe( changed( ruta.imagenes.dist ) )

		.pipe( imageop({
			optimizationLevel: 5,
			progressive: true,
			interlaced: true
		}))

		.pipe( gulp.dest( ruta.imagenes.dist ) )

		.on("end", function(){

			gulp.start("imagenessvg")

		})
		.pipe( notify("Imagen optimizada: <%= file.relative %>") );
		
});





/*	IMAGENES SVG
----------------------------------------------------------------------*/
gulp.task( "imagenessvg",  function(){


	gulp.src( carpeta.fuente + "/images/**/*.svg" )
		
		// Cambiar de imagen.svg -> imagen.[ID].svg
		.pipe( produccion( cachebust.resources() ) )

		/**
		 *
		 * == collate(), deleted() y changed()
		 * Verificar si existe un cambio o se ha eliminado el archivo 
		 * para efectuar su cambio o eliminación de la carpeta /dist
		 *
		 */
		.pipe( collate( carpeta.fuente + "/images" ) )
		.pipe( deleted( ruta.imagenes.dist, ["**/*.svg"]) )
		.pipe( changed( ruta.imagenes.dist ) )

		.pipe( gulp.dest( ruta.imagenes.dist ) )

		.on("end", function(){

			gulp.start("estilos:cache_imagenes")

		})
		
});





/*	ESTILOS CON CACHÉ DE IMAGENES
	------------------------------------------------------------------
	Se trae el `hash` de las imágenes guardadas en la tarea 'IMAGENES' 
	y recorre todos los archivos .styl para duplicarlos en la carpeta
	temporal '.temp' y así poder modificar los nombres de cada
	imagen con el `hash`
----------------------------------------------------------------------*/
gulp.task( "estilos:cache_imagenes", function(){

	gulp.src( ruta.estilos.watch )

		// Evitar que se detenga gulp.watch al encontrar error
		.pipe(plumber())

		/**
		 *
		 *	Extrae el `hash` de cada imagen y lo agrega a al código
		 *	en cada ruta de archivo.
		 *
		 */
		.pipe( produccion( cachebust.references() ) )

		.pipe( gulp.dest( carpeta.fuente + "/styl/.temp" ) )

		.on("end", function(){

			gulp.start("estilos")

		})

});



/*	ESTILOS
	------------------------------------------------------------------
	Compila CSS en un archivo final minificado y con un `hash` en el
	nombre del archivo para evitar caché, ej: global.min.e568d123.css
----------------------------------------------------------------------*/
gulp.task( "estilos", function(){

	gulp.src( carpeta.fuente + "/styl/.temp/global.styl" )

		// Evitar que se detenga gulp.watch al encontrar error
		.pipe(plumber())

		.pipe( sourcemaps.init() )

		.pipe( styl({
			use: [ nib(), rupture()],
			"include css": true,
			//compress: true,
			//linenos: true
		}))
		.pipe( produccion( minifyCSS() ) )

		/*.pipe( produccion( rename({
			suffix: ".min"
		}) ))*/
		
		.pipe( deleted( ruta.estilos.dist ,["**/*"]) )

		// Generar `hash`en el archivo
		.pipe( produccion( cachebust.resources() ) )

		.pipe( produccion( header(fs.readFileSync( carpeta.fuente + '/copyright-css.txt', 'utf8'), { pkg : pkg, year: ano } ) ))
		.pipe( desarrollo( sourcemaps.write(".") ) )

		.pipe( gulp.dest( ruta.estilos.dist ) )

		.on("end", function(){

			gulp.start("jade")

		})

		// Borra todos los archivos de la carpeta '.temp'
		.pipe( deleted( carpeta.fuente + "/styl/.temp" ,["**/*"]) )
		
		.pipe( notify("Stylus compilado: <%= file.relative %>") );

});






/*	TEMPLATES
	------------------------------------------------------------------
	Copia todos los archivos 'html,html,tpl,php' en la carpeta `dist`
	y modifica el código de las rutas CSS y JS para que conincidan 
	con el `hash` del archivo que se generó previamente.
----------------------------------------------------------------------*/
gulp.task("jade", function(){

	gulp.src( ruta.tpl.src )

		// Evitar que se detenga gulp.watch al encontrar error
		.pipe(plumber())

		.pipe( jade() )

		.pipe(prettify({
			indent_size: 4
		}))

		.pipe( produccion( cachebust.references() ) )

		/**
		 *
		 * == collate(), deleted() y changed()
		 * Verificar si existe un cambio o se ha eliminado el archivo 
		 * para efectuar su cambio o eliminación de la carpeta /dist
		 *
		 */
		.pipe( collate( carpeta.fuente + "/tpl" ) )
		.pipe( deleted( carpeta.destino, ["**/*.{htm,html,php,tpl,jade}"]) )
		// .pipe( changed( carpeta.destino ) )

		.pipe( gulp.dest( carpeta.destino ) )


		.on("end", function(){

			gulp.start("css-inline")

		})

});





/*	^CSS INLINE
-------------------------------------------------------------*/

gulp.task("css-inline", function(){

	gulp.src("dist/*.html")

		// Evitar que se detenga gulp.watch al encontrar error
		.pipe(plumber())

		.pipe( inlineCss() )

		.pipe( gulp.dest("dist/emails-production") )

		.pipe( browserSync.stream() )

});











/*	CREAR:
		- .htaccess
		- mantenimiento.desactivar
	------------------------------------------------------------------
	Esta tarea no se ejecuta por defecto ya que es solo para copiar
	los archivos en la carpeta /dist
----------------------------------------------------------------------*/
gulp.task( "htaccess", function(){

	/*
	 *	.htaccess contiene configuraciones del servidor Apache.
	 */
	gulp.src( carpeta.fuente + "/.htaccess" )
		.pipe( gulp.dest( carpeta.destino ) )


	/*
	 *	Con este archivo luego se podrá cambiar el estado a mantenimiento
	 *	del sitio completo. Revisar instrucciones en el archivo .htaccess
	 *	en la sección PÁGINA EN MANTENIMIENTO
	 */
	gulp.src( carpeta.fuente + "/mantenimiento.desactivar" )
		.pipe( gulp.dest( carpeta.destino ) )


});






/*	VIGILAR, ESCUCHAR Y CHISMOCIAR :D
----------------------------------------------------------------------*/
gulp.task("watch", function(){

	gulp.watch( ruta.estilos.watch, ["estilos:cache_imagenes"] );

	gulp.watch( ruta.imagenes.src, ["imagenes"] );

	gulp.watch( carpeta.fuente + '/images/**/*.svg', ["imagenes"] );

	gulp.watch( carpeta.fuente + '/tpl/**/*.jade', ["jade"] );

});




/*	BUILD
	------------------------------------------------------------------
	Como ves la tarea 'estilos' no se ejecuta directamente, pero a
	tráves de la tarea 'imagenes' luego se ejecutan los 'estilos'
----------------------------------------------------------------------*/
gulp.task( "build", ["imagenes"] );


/*	EJECUTAR POR DEFECTO
----------------------------------------------------------------------*/
gulp.task( "default", ["servidor", "build", "watch"] );



